﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Basket.Database;
using Basket.Models;

namespace Basket.Services
{
    public class BasketService : IBasketService
    {
        private readonly IDataBaseContext _database;
        private readonly Dictionary<ItemKeyEnum, double> _itemDictionary;

        public BasketService(IDataBaseContext database)
        {
            _database = database;
            _itemDictionary = new Dictionary<ItemKeyEnum, double>
            {
                {ItemKeyEnum.Bread, 1.0},
                {ItemKeyEnum.Milk, 1.15},
                {ItemKeyEnum.Butter, 0.80}
            };
        }

        public Dictionary<ItemKeyEnum, double> GetItems()
        {
            return _itemDictionary;
        }

        public async Task<AmountResponseModel> UpdateOrder(string id, ShoppingBasket input)
        {
            if (input.Items.Any(a => a.Quantity < 0))
            {
                throw new ArgumentOutOfRangeException("item.quantity", "Quantity of Items cannot be negative.");
            }

            var shoppingBasket = _database.Query<ShoppingBasket>().FirstOrDefault(a => a.Id == id);
            if (shoppingBasket == null)
            {
                throw new ArgumentNullException(
                    $"basket with id:{id} does not exist.");
            }

            shoppingBasket.Items = input.Items;
            shoppingBasket.Name = input.Name;
            var total = CalculateBasketValue(shoppingBasket);
            shoppingBasket.Discount = total.Discount;
            shoppingBasket.Total = total.Total;
            await _database.SaveAsync();
            return total;
        }

        public async Task<bool> DeleteBasket(string id)
        {
            var shoppingBasket = _database.Query<ShoppingBasket>().FirstOrDefault(a => a.Id == id);
            if (shoppingBasket == null)
            {
                throw new ArgumentNullException("id",
                    $"Basket with id:{id} does not exist.");
            }
            await _database.DeleteEntity(shoppingBasket.Id);
            return true;
        }

        public List<ShoppingBasket> GetOrders()
        {
            return _database.Query<ShoppingBasket>().ToList();
        }

        public ShoppingBasket GetOrder(string id)
        {
            return _database.Query<ShoppingBasket>().FirstOrDefault(a => a.Id == id);
        }

        public AmountResponseModel GetOrderAmount(string basketId)
        {
            var shoppingBasket = _database.Query<ShoppingBasket>().FirstOrDefault(a => a.Id == basketId);
            if (shoppingBasket == null)
            {
                throw new ArgumentNullException(
                    $"basket with id:{basketId} does not exist.");
            }

            return new AmountResponseModel(shoppingBasket.Discount, shoppingBasket.Total);
        }

        public async Task<string> CreateOrder(ShoppingBasket input)
        {
            if (input.Items.Any(a => a.Quantity < 0))
            {
                throw new ArgumentOutOfRangeException("item.quantity", "Quantity of Items cannot be negative.");
            }

            if (!string.IsNullOrEmpty(input.Id))
            {
                var basket = _database.Query<ShoppingBasket>().FirstOrDefault(a => a.Id == input.Id);
                if (basket != null)
                {
                    throw new ArgumentException(
                        $"basket with id:{basket.Id} already exists, please use PUT method to edit basket.");
                }
            }

            var amount = CalculateBasketValue(input);
            input.Total = amount.Total;
            input.Discount = amount.Discount;
            await _database.AddAsync(input);
            await _database.SaveAsync();

            return input.Id;
        }

        public AmountResponseModel CalculateBasketValue(ShoppingBasket input)
        {
            double total = 0;
            double discount = 0;
            input.Items.ForEach(item =>
            {
                _itemDictionary.TryGetValue(item.Key, out var value);
                total += item.Quantity * value;
                switch (item.Key)
                {
                    case ItemKeyEnum.Butter:
                        _itemDictionary.TryGetValue(ItemKeyEnum.Bread, out var discountValue);
                        discount += discountValue / 2 * (item.Quantity / 2);
                        break;
                    case ItemKeyEnum.Milk:
                        discount += value * (item.Quantity / 4);
                        break;
                }
            });
            return new AmountResponseModel(Math.Round(discount, 2), Math.Round(total, 2));
        }
    }
}
