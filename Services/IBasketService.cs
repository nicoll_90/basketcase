﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Basket.Models;

namespace Basket.Services
{
    public interface IBasketService
    {
        Dictionary<ItemKeyEnum, double> GetItems();
        
        AmountResponseModel GetOrderAmount(string basketId);
        Task<string> CreateOrder(ShoppingBasket input);
        Task<AmountResponseModel> UpdateOrder(string id, ShoppingBasket input);
        Task<bool> DeleteBasket(string id);
        List<ShoppingBasket> GetOrders();
        ShoppingBasket GetOrder(string id);
    }
}
