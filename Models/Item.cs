﻿namespace Basket.Models
{
    public class Item : Entity
    {
        public ItemKeyEnum Key { get; set; }
        public int Quantity { get; set; }
    }
}
