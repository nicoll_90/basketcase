﻿using System;
using System.Collections.Generic;

namespace Basket.Models
{
    public class ShoppingBasket : Entity
    {
        public List<Item> Items { get; set; }
        public double Total { get; set; }
        public double Discount { get; set; }
        public string Name { get; set; }
        public Guid UserId { get; set; }
    }
}
