﻿namespace Basket.Models
{
    public class AmountResponseModel
    {
        public AmountResponseModel(double discount, double total) : this()
        {
            Total = total;
            Discount = discount;
        }

        public AmountResponseModel()
        {
        }

        public double Total { get; set; }
        public double Discount { get; set; }
    }
}
