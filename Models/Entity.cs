﻿using System;

namespace Basket.Models
{
    public class Entity
    {
        public Entity()
        {
            Id =  Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
    }
}
