﻿namespace Basket.Models
{
    public enum ItemKeyEnum
    {
        Bread,
        Milk,
        Butter
    }
}