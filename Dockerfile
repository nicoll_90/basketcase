FROM microsoft/dotnet:2.1-sdk as builder

RUN mkdir -p /builder/basket
RUN mkdir -p /builder/Release
WORKDIR /builder/basket

COPY . .

RUN dotnet restore Basket.sln
RUN dotnet build Basket.sln

RUN dotnet test Basket.Tests/Basket.Tests.csproj

RUN dotnet publish -c Release -o /builder/Release

FROM microsoft/dotnet:2.1-aspnetcore-runtime-alpine3.7

RUN mkdir /app
COPY --from=builder /builder/Release/ /app
WORKDIR /app

ENTRYPOINT [ "dotnet", "Basket.dll" ]
