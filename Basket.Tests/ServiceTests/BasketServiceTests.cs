﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Basket.Database;
using Basket.Models;
using Basket.Services;
using FluentAssertions;
using Moq;
using Xunit;

namespace Basket.Tests.ServiceTests
{
    public class BasketServiceTests
    {
        private readonly Mock<IDataBaseContext> _dataContectMock;

        private readonly BasketService _target;

        public BasketServiceTests()
        {
            _dataContectMock = new Mock<IDataBaseContext>();
            _target = new BasketService(_dataContectMock.Object);
        }

        [Theory]
        [InlineData(2, 0, 2, 0.5, 3.10)]
        [InlineData(1, 1, 1, 0, 2.95)]
        [InlineData(0, 4, 0, 1.15, 3.45)]
        [InlineData(2, 4, 0, 1.15, 5.45)]
        [InlineData(1, 8, 2, 2.80, 9)]
        [InlineData(1, 12, 2, 3.95, 12.45)]
        public void Service_Should_Yield_expected_TotalAndDiscount(
            int breadQuantity,
            int milkQuantity,
            int butterQuantity,
            double expectedDiscount, double expectedTotal)
        {
            //Setup
            var userId = Guid.NewGuid();

            var expectedTotalWithoutDiscount = Math.Round(expectedTotal + expectedDiscount, 2);
            var order = GenerateOrder(breadQuantity, butterQuantity, milkQuantity, userId);

            _dataContectMock.Setup(a => a.Query<ShoppingBasket>())
                .Returns(new List<ShoppingBasket>
                {
                    order
                }.AsQueryable());

            //Act
            var result = _target.CalculateBasketValue(order);
            //Assert
            result.Should().NotBeNull();
            result.Total.Should().Be(expectedTotalWithoutDiscount);
            result.Discount.Should().Be(expectedDiscount);
            var totalWithDiscount = Math.Round(result.Total - result.Discount, 2);
            totalWithDiscount.Should().Be(expectedTotal);
        }

        private ShoppingBasket GenerateOrder(int breadQuantity, int butterQuantity, int milkQuantity,
            Guid? userId)
        {
            return new ShoppingBasket
            {
                Discount = 100,
                Name = "Something",
                UserId = userId ?? Guid.NewGuid(),
                Items = new List<Item>
                {
                    new Item
                    {
                        Key = ItemKeyEnum.Butter,
                        Quantity = butterQuantity
                    },
                    new Item
                    {
                        Key = ItemKeyEnum.Milk,
                        Quantity = milkQuantity
                    },
                    new Item
                    {
                        Key = ItemKeyEnum.Bread,
                        Quantity = breadQuantity
                    }
                }
            };
        }

        [Fact]
        public void GetAmount_From_Service()
        {
            const double expectedTotal = 2.0;
            const double expectedDiscount = 0.0;
            var basket = GenerateOrder(2, 0, 0, Guid.NewGuid());
            basket.Total = expectedTotal;
            basket.Discount = expectedDiscount;
            _dataContectMock.Setup(a => a.Query<ShoppingBasket>()).Returns(new List<ShoppingBasket>
            {
                basket
            }.AsQueryable());

            //Act
            var result = _target.GetOrderAmount(basket.Id);

            result.Should().NotBeNull();
            result.Total.Should().Be(expectedTotal);
            result.Discount.Should().Be(expectedDiscount);
        }

        [Fact]
        public async Task Update_Order_Successfully()
        {
            var expectedTotal = Math.Round(6.6, 2);
            var expectedDiscount = Math.Round(1.15, 2);
            var basket = GenerateOrder(2, 0, 0, Guid.NewGuid());
            basket.Items.FirstOrDefault(a => a.Key == ItemKeyEnum.Milk).Quantity = 4;

            _dataContectMock.Setup(a => a.Query<ShoppingBasket>()).Returns(new List<ShoppingBasket>
            {
                basket
            }.AsQueryable());

            //Act
            var result = await _target.UpdateOrder(basket.Id, basket);

            result.Should().NotBeNull();
            result.Total.Should().Be(expectedTotal);
            result.Discount.Should().Be(expectedDiscount);
            _dataContectMock.Verify(a => a.SaveAsync(CancellationToken.None), Times.Once);
        }

        [Fact]
        public async Task Update_Order_With_Negatives_Unsuccessfully()
        {
            //Setup
            var basket = GenerateOrder(2, 0, 0, Guid.NewGuid());
            basket.Items.FirstOrDefault(a => a.Key == ItemKeyEnum.Milk).Quantity = -4;
            //Act
            try
            {
                await _target.UpdateOrder(basket.Id, basket);
            }
            catch (ArgumentOutOfRangeException e)
            {
                //Assert
                e.ParamName.Should().Be("item.quantity");
            }

            _dataContectMock.Verify(a => a.SaveAsync(CancellationToken.None), Times.Never);
        }


        [Fact]
        public async Task Delete_Order_Successfully()
        {
            //Setup
            var basket = GenerateOrder(2, 0, 0, Guid.NewGuid());
            _dataContectMock.Setup(a => a.Query<ShoppingBasket>()).Returns(new List<ShoppingBasket>()
            {
                basket
            }.AsQueryable());
            
            //Act
            var result = await _target.DeleteBasket(basket.Id);

            //Assert
            result.Should().BeTrue();
            _dataContectMock.Verify(a => a.DeleteEntity(It.IsAny<string>()), Times.Once);
            _dataContectMock.Verify(a => a.Query<ShoppingBasket>(), Times.Once);
        }

        [Fact]
        public async Task Delete_Order_That_Doesnt_Exists_Fails()
        {
            //Setup
            var basket = GenerateOrder(2, 0, 0, Guid.NewGuid());

            //Act
            try
            {
                await _target.DeleteBasket(basket.Id);
            }
            catch (ArgumentNullException e)
            {
                //Assert
                e.Message.StartsWith($"Basket with id:{basket.Id} does not exist.").Should().BeTrue();
                _dataContectMock.Verify(a => a.DeleteEntity(It.IsAny<string>()), Times.Never);
                _dataContectMock.Verify(a => a.Query<ShoppingBasket>(), Times.Once);
                _dataContectMock.Verify(a => a.SaveAsync(CancellationToken.None), Times.Never);
            }
        }
    }
}
