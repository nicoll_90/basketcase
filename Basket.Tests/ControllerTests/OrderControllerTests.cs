﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Basket.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Basket.Tests.ControllerTests
{
    public class OrderControllerTests : BaseTestController
    {
        [Fact]
        public async Task Get_Basket_By_Id()
        {
            //Setup
            const int expecteditemquantity = -2;
            const ItemKeyEnum expecteditemType = ItemKeyEnum.Bread;
            var basket = GenerateShoppingBaskets(expecteditemquantity, expecteditemType);

            BasketServiceMock.Setup(a => a.CreateOrder(basket)).ThrowsAsync(
                new ArgumentOutOfRangeException("item.quantity", "Quantity of Items cannot be negative."));

            //Act
            try
            {
                await OrderController.Post(basket);
            }
            catch (ArgumentOutOfRangeException e)
            {
                //Asset
                e.ParamName.Should().Be("item.quantity");
            }
        }

        [Fact]
        public void Get_Order_Amount_Should_respond_Successfully()
        {
            //Setup
            const double expectedTotal = 2.0;
            const double expectedDiscount = 0.0;
            var basket = GenerateShoppingBaskets(2, ItemKeyEnum.Bread);
            basket.Total = expectedTotal;
            basket.Discount = expectedDiscount;
            BasketServiceMock.Setup(a => a.GetOrderAmount(basket.Id))
                .Returns(new AmountResponseModel(expectedDiscount, expectedTotal));

            //Act
            var result = OrderController.GetAmount(basket.Id);
            var objectResult = result.As<OkObjectResult>();
            objectResult.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var response = objectResult.Value as AmountResponseModel;

            response.Should().NotBeNull();
            response.Total.Should().Be(expectedTotal);
            response.Discount.Should().Be(expectedDiscount);
        }

        [Fact]
        public void GetItems_Should_return_items_on_sale()
        {
            //Setup
            const int expectedCount = 3;
            var expectedResult = new Dictionary<ItemKeyEnum, double>
            {
                {ItemKeyEnum.Bread, 1.0},
                {ItemKeyEnum.Milk, 1.15},
                {ItemKeyEnum.Butter, 0.80}
            };

            BasketServiceMock.Setup(a => a.GetItems()).Returns(expectedResult);

            //Act
            var result = OrderController.GetItems();
            var objectResult = result.As<OkObjectResult>();
            objectResult.StatusCode.Should().Be((int) HttpStatusCode.OK);
            var basket = objectResult.Value as Dictionary<ItemKeyEnum, double>;
            basket.Should().NotBeEmpty();
            basket.Count.Should().Be(expectedCount);
        }

        [Fact]
        public async Task PostNewBasket_Should_return_successfully()
        {
            //Setup
            const string expectedId = "newId";
            var basket = GenerateShoppingBaskets(4, ItemKeyEnum.Milk);
            BasketServiceMock.Setup(a => a.CreateOrder(basket)).ReturnsAsync(expectedId);
            //Act
            var result = await OrderController.Post(basket);
            var objectResult = result.As<OkObjectResult>();
            objectResult.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //Asset
            var shoppingBasketId = objectResult.Value as string;

            shoppingBasketId.Should().NotBeNull();
            shoppingBasketId.Should().Be(expectedId);
        }

        [Fact]
        public async Task PostNewBasket_WithNegativeQuantityItems_Should_return_Error()
        {
            //Setup
            const int expecteditemquantity = -2;
            const ItemKeyEnum expecteditemType = ItemKeyEnum.Bread;
            var basket = GenerateShoppingBaskets(expecteditemquantity, expecteditemType);

            BasketServiceMock.Setup(a => a.CreateOrder(basket)).ThrowsAsync(
                new ArgumentOutOfRangeException("item.quantity", "Quantity of Items cannot be negative."));

            //Act
            try
            {
                await OrderController.Post(basket);
            }
            catch (ArgumentOutOfRangeException e)
            {
                //Asset
                e.Message.StartsWith("Quantity of Items cannot be negative.").Should().BeTrue();
                e.ParamName.Should().Be("item.quantity");
            }
        }


        [Fact]
        public async Task Update_Basket_with_Extra_items_successfully()
        {
            //Setup
            var expectedDiscount = Math.Round(1.15, 2);
            var expectedTotal = Math.Round(6.6, 2);
            const int expecteditemquantity = 2;
            var basketId = Guid.NewGuid().ToString();
            const ItemKeyEnum expecteditemType = ItemKeyEnum.Bread;
            var basket = GenerateShoppingBaskets(expecteditemquantity, expecteditemType);
            basket.Id = basketId;

            basket.Id = basketId;
            basket.Items.Add(new Item
            {
                Key = ItemKeyEnum.Milk,
                Quantity = 4
            });

            BasketServiceMock.Setup(a => a.UpdateOrder(basketId, basket))
                .ReturnsAsync(new AmountResponseModel(expectedDiscount, expectedTotal));

            //Act
            var result = await OrderController.Put(basket, basket.Id);
            var objectResult = result.As<OkObjectResult>();
            objectResult.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var response = objectResult.Value as AmountResponseModel;

            response.Total.Should().Be(expectedTotal);
            response.Discount.Should().Be(expectedDiscount);
        }

        [Fact]
        public async Task Update_Basket_with_Extra_items_With_Negative_Quantities_Soesnt_Save()
        {
            //Setup
            const int expecteditemquantity = 2;
            const ItemKeyEnum expecteditemType = ItemKeyEnum.Bread;
            var basket = GenerateShoppingBaskets(expecteditemquantity, expecteditemType);
            basket.Id = Guid.NewGuid().ToString();

            //Act
            var result = await OrderController.Put(basket, basket.Id);
            var objectResult = result.As<OkObjectResult>();
            objectResult.StatusCode.Should().Be((int) HttpStatusCode.OK);
            var response = objectResult.Value as Dictionary<ItemKeyEnum, double>;
        }
    }
}
