﻿using System.Collections.Generic;
using System.Linq;
using Basket.Controllers;
using Basket.Database;
using Basket.Models;
using Basket.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Moq;

namespace Basket.Tests.ControllerTests
{
    public class BaseTestController
    {
        protected readonly Mock<IBasketService> BasketServiceMock;
        protected readonly Mock<IDataBaseContext> DatabaseMock;
        protected OrderController OrderController;

        public BaseTestController()
        {
            var actionDescriptor = new Mock<ActionDescriptor>();
            actionDescriptor.SetupGet(x => x.DisplayName).Returns("Action_With_SomeAttribute");
            DatabaseMock = new Mock<IDataBaseContext>();
            BasketServiceMock = new Mock<IBasketService>();
            OrderController = SetupOrderController(actionDescriptor.Object);
        }

        private OrderController SetupOrderController(ActionDescriptor actionDescriptor)
        {
            OrderController =
                new OrderController(BasketServiceMock.Object)
                {
                    ControllerContext = new ControllerContext()
                };
            OrderController.ControllerContext.HttpContext = new DefaultHttpContext();
            var httpActionContext = new ActionExecutingContext(
                new ActionContext
                {
                    HttpContext = OrderController.HttpContext,
                    RouteData = new RouteData(),
                    ActionDescriptor = actionDescriptor
                }, new Mock<List<IFilterMetadata>>().Object,
                new Mock<IDictionary<string, object>>().Object,
                OrderController);
            OrderController.OnActionExecuting(httpActionContext);
            return OrderController;
        }

        protected ShoppingBasket GenerateShoppingBaskets(int number, ItemKeyEnum type)
        {
            return new ShoppingBasket
            {
                Discount = 0,
                Items = new List<Item>
                {
                    new Item
                    {
                        Key = type,
                        Quantity = number
                    }
                }
            };
        }
    }
}
