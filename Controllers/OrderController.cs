﻿using System.Threading.Tasks;
using Basket.Models;
using Basket.Services;
using Microsoft.AspNetCore.Mvc;

namespace Basket.Controllers
{
    [Route("[controller]")]
    public class OrderController : Controller
    {
        private readonly IBasketService _basketService;

        public OrderController(IBasketService basketService)
        {
            _basketService = basketService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ShoppingBasket input)
        {
            return new OkObjectResult(await _basketService.CreateOrder(input));
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Put([FromBody] ShoppingBasket input, [FromRoute] string id)
        {
            return new OkObjectResult(await _basketService.UpdateOrder(id, input));
        }

        [HttpGet]
        [Route("{id}")]
        [Route("")]
        public IActionResult Get([FromRoute] string id = null)
        {
            return string.IsNullOrEmpty(id)
                ? new OkObjectResult(_basketService.GetOrders())
                : new OkObjectResult(_basketService.GetOrder(id));
        }    
        
        [HttpGet]
        [Route("{id}/amount")]
        public IActionResult GetAmount([FromRoute] string id)
        {
            return new OkObjectResult(_basketService.GetOrderAmount(id));
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            await _basketService.DeleteBasket(id);
            return new NoContentResult();
        }

        [HttpGet]
        [Route("items")]
        public IActionResult GetItems()
        {
            return new OkObjectResult(_basketService.GetItems());
        }
    }
}
