﻿using Autofac;
using Basket.Database;
using Basket.Services;

namespace Basket.Modules
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            Configure(builder);
        }

        private static void Configure(ContainerBuilder builder)
        {
            builder.RegisterType<DocumentStoreHolder>()
                .As<IDocumentStoreHolder>()
                .SingleInstance();

            builder.RegisterType<DataBaseContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<BasketService>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
