﻿using System;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;
using Serilog;

namespace Basket.Database
{
    public class DocumentStoreHolder : IDocumentStoreHolder
    {
        private readonly ILogger _logger = Log.ForContext<DocumentStoreHolder>();

        public DocumentStoreHolder(IOptions<AppSettings> appSettings)
        {
            var settings = appSettings.Value;

            //To Create initial Database
            var localStore = new DocumentStore
            {
                Urls = new[] {settings.RavenConnectionString}
            }.Initialize();
            try
            {
                localStore.Maintenance.Server.Send(
                    new CreateDatabaseOperation(new DatabaseRecord(settings.DefaultDatabase)));
            }
            catch (Exception e)
            {
                //If already created dont actually through error
                localStore.Dispose();
                _logger.Warning($"Database {settings.DefaultDatabase} already exists!");
            }

            //Set Store for DI
            Store = new DocumentStore
            {
                Database = settings.DefaultDatabase,
                Urls = new[] {settings.RavenConnectionString}
            }.Initialize();
        }

        private IDocumentStore Store { get; }

        public IDocumentStore RavenStore()
        {
            return Store;
        }
    }
}
