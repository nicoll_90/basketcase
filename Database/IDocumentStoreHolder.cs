﻿using Raven.Client.Documents;

namespace Basket.Database
{
    public interface IDocumentStoreHolder
    {
        IDocumentStore RavenStore();
    }
}
