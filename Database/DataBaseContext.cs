﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Raven.Client.Documents.Session;
using Serilog;

namespace Basket.Database
{
    public class DataBaseContext : IDataBaseContext
    {
        private readonly ILogger _logger = Log.ForContext<DataBaseContext>();
        private readonly IAsyncDocumentSession _asyncSession;
        private readonly IDocumentSession _syncSession;

        public DataBaseContext(IDocumentStoreHolder database)
        {
            var store = database.RavenStore();
            _asyncSession = store.OpenAsyncSession();
            _syncSession = store.OpenSession();
        }


        public Task SaveAsync(CancellationToken token = default(CancellationToken))
        {
            return _asyncSession.SaveChangesAsync(token);
        }

        public Task AddAsync<T>(T entity, CancellationToken token = default(CancellationToken)) where T : class, new()
        {
            return _asyncSession.StoreAsync(entity, token);
        }

        public async Task DeleteEntity(string id)
        {
            _asyncSession.Delete(id);
            await _asyncSession.SaveChangesAsync();
        }

        public IQueryable<T> Query<T>() where T : class, new()
        {
            return _syncSession.Query<T>();
        }
    }

    public interface IDataBaseContext
    {
        Task DeleteEntity(string id);
        Task SaveAsync(CancellationToken token = default(CancellationToken));
        Task AddAsync<T>(T entity, CancellationToken token = default(CancellationToken)) where T : class, new();
        IQueryable<T> Query<T>() where T : class, new();
    }
}
