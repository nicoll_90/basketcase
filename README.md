# BasketCase

______

## Running the app with Docker
* Using the commandline go to git repo
* By default the .env is setup correctly so just do `docker-compose up -d`
1. DB port is 8080 by default
1. Backend port is 5000


## Running the app with Visual Studio/Rider
* Business as usual

## Running the app with the cmd-line
1. cd git repo
1. dotnet run -c Release

### Run tests with the cmd-line
1. cd git repo
1. cd into Basket.Tests
1. dotnet test

## API requests

I recommend using Postman for the API requests and I have added a PostMan folder in the git repo with pre defined API's
https://www.getpostman.com/apps


1. Basket.postman_collection.json is the API requests
1. Development.postman_environment.json is the pre-defined env using backend port 5000

#### Assumptions/Minutiae
* No need for Frontend
* FE can figure out the discounted price by doing some math
* UserId in ShoppingBasket will be used when there is some user context, Would have done some more middleware's but I think I was getting carried away.
* I used RavenDB (first time ever so please dont judge).
* Repository can be found here https://bitbucket.org/nicoll_90/basketcase
* I used BitBucket Pipeline for the first time so please see that here https://bitbucket.org/nicoll_90/basketcase/addon/pipelines/home#!/results/12
* * These images can also be found in my hub.docker.com account here https://hub.docker.com/r/android101/random/
  * docker-compose.yml file uses public image `android101/random:12`