﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Basket.Middlewares
{
    public class ExceptionMiddleware
    {
        private static readonly ILogger Logger = Log.ForContext<ExceptionMiddleware>();

        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                Logger.Error(e, e.Message);
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync($"Exception:{e.Message}");
            }
        }
    }
}