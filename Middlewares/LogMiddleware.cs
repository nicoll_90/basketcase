﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;

namespace Basket.Middlewares
{
    public class LogMiddleware
    {
        private static readonly ILogger Logger = Log.ForContext<LogMiddleware>();

        private readonly RequestDelegate _next;

        public LogMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            Logger.Debug($"Req: {context.Request.Method} {context.Request.Path}");
            await _next(context);
        }
    }
}
