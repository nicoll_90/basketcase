namespace Basket
{
    public class AppSettings
    {
        public string ServiceUrl { get; set; }
        public string HostName { get; set; }
        public string RavenConnectionString { get; set; }
        public string DefaultDatabase { get; set; }
    }
}
