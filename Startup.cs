using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using Basket.Middlewares;
using Basket.Modules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using Serilog.Filters;
using ILogger = Serilog.ILogger;

namespace Basket
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            SetDefaultApplicationConfiguration();

            Configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables().Build();

            Log.Logger = ConfigureLogger(Configuration);
        }

        public IConfigurationRoot Configuration { get; }

        public IContainer ApplicationContainer { get; private set; }

        private static ILogger ConfigureLogger(IConfiguration configuration)
        {
            // 100 MB
            const long fileSizeLimit = 100000000;
            var loggerConfiguration =
                new LoggerConfiguration()
                    .MinimumLevel.Verbose()
                    .WriteTo.Async(b => b.Logger(l =>
                            l.Enrich.FromLogContext())
                        .WriteTo.Async(a => a
                            .File("logs/info.log", rollOnFileSizeLimit: true, fileSizeLimitBytes: fileSizeLimit,
                                rollingInterval: RollingInterval.Hour, retainedFileCountLimit: 3)
                            .Filter.ByIncludingOnly(Matching.FromSource("Basket"))))
                    .ReadFrom.Configuration(configuration);

            return loggerConfiguration.CreateLogger();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddRouting(opt => opt.LowercaseUrls = true);

            // Add framework services.
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.Formatting = Formatting.Indented;
                    options.SerializerSettings.TypeNameHandling = TypeNameHandling.None;
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.Converters = new List<JsonConverter>
                    {
                        new StringEnumConverter()
                    };
                });

            services.Configure<AppSettings>(options => Configuration.GetSection("AppSettings").Bind(options));

            var builder = new ContainerBuilder();
            builder.RegisterLogger();
            builder.RegisterInstance(Configuration);
            builder.RegisterModule<ServiceModule>();

            builder.Populate(services);
            ApplicationContainer = builder.Build();

            PrintSettings();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IApplicationLifetime appLifetime)
        {
            //Request Pipeline => Order matters here!!!!
            appLifetime.ApplicationStopped.Register(OnApplicationStopped);
            loggerFactory.AddSerilog();
            app.UseMiddleware<LogMiddleware>();
            app.UseMiddleware<ExceptionMiddleware>();

            app.UseMvc();
        }

        private void OnApplicationStopped()
        {
            Log.CloseAndFlush();
            ApplicationContainer.Dispose();
        }

        private void PrintSettings()
        {
            var appsettings = ApplicationContainer.Resolve<IOptions<AppSettings>>();
            Log.Logger.ForContext<Startup>()
                .Information(JsonConvert.SerializeObject(appsettings.Value, Formatting.None));
        }

        private static void SetDefaultApplicationConfiguration()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Converters = new List<JsonConverter>
                {
                    new StringEnumConverter()
                },
                TypeNameHandling = TypeNameHandling.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize
            };
        }
    }
}
